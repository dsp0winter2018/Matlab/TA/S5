clear all;close all
clc;
% DSP BSC course
% Demo program by Hamid Sheikhzadeh, Nov. 2017
% IIR Butterworth filter design in cascade.
%  FAILS with lower number of bits!
FiltOrd= 4;
Wn=[.15 .20]; % try [.15 .18], [.15 .20], and [.15 .25] too.
N=1024;
for Bits=[ 8 12 16 24 ]
    Sc=2^Bits;
    [B,A]=butter(FiltOrd,Wn);
    Ra=roots(A)
    A1=round(poly(Ra(1:2))*Sc)/Sc;
    A2=round(poly(Ra(3:4))*Sc)/Sc;
    A3=round(poly(Ra(5:6))*Sc)/Sc;
    A4=round(poly(Ra(7:8))*Sc)/Sc;
    %
    Aq=round(conv(A1,A2)*Sc)/Sc;
    Aq=round(conv(Aq,A3)*Sc)/Sc;
    Aq=round(conv(Aq,A4)*Sc)/Sc;
    %
    Rb=roots(B);
    B1=round(poly(Rb(1:2))*Sc)/Sc;
    B2=round(poly(Rb(3:4))*Sc)/Sc;
    B3=round(poly(Rb(5:6))*Sc)/Sc;
    B4=round(poly(Rb(7:8))*Sc)/Sc;
    %
    Bq=round(conv(B1,B2)*Sc)/Sc;
    Bq=round(conv(Bq,B3)*Sc)/Sc;
    Bq=round(conv(Bq,B4)*Sc)/Sc;
    %    
    [H,w]=freqz(B,A,N);
    H=H/max(abs(H)); % Max to 0 dB
    [Hq,w]=freqz(Bq,Aq,N);
    Hq=Hq/max(abs(Hq)); % Max to 0 dB
    figure;
    plot(w,20*log10(abs(H)),'b.-')
    grid on; hold on
    plot(w,20*log10(abs(Hq)),'r.-')
    title(['Freq Resp (dB) for ',num2str(Bits),' Bits']);
    figure
    Rq=(roots(Aq));
    polar(angle(Rq),abs(Rq),'ro');
    if max(abs(Rq))>= 1,
        disp(['Unstable filter for ',num2str(Bits),' Bits, Freq Resp Invalid!']);
        title(['Unstable Poles for ',num2str(Bits),' Bits']);
    else
        title(['Poles for ',num2str(Bits),' Bits']);
    end;
    hold on;grid on;
    R=(roots(A));
    polar(angle(R),abs(R),'b*');
 end;