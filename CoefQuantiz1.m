clear all;close all
clc;
% DSP BSC course
% Demo program by Hamid Sheikhzadeh, Nov. 2017
% IIR Butterworth filter design.
%  FAILS with lowe number of bits!
FiltOrd= 8;
Wn=[.15 .20]; % try [.15 .18], [.15 .20], and [.15 .25] too.
N=1024;
for Bits=[12 16 24 32 64]
    Sc=2^Bits;
    [B,A]=butter(FiltOrd,Wn);
    Aq= round(A * Sc)/Sc;
    Bq= round(B * Sc)/Sc;
    [H,w]=freqz(B,A,N);
    H=H/max(abs(H)); % Max to 0 dB
    [Hq,w]=freqz(Bq,Aq,N);
    Hq=Hq/max(abs(Hq)); % Max to 0 dB
    figure;
    plot(w,20*log10(abs(H)),'b.-')
    grid on; hold on
    plot(w,20*log10(abs(Hq)),'r.-')
    title(['Freq Resp (dB) for ',num2str(Bits),' Bits']);
    figure
    Rq=(roots(Aq));
    polar(angle(Rq),abs(Rq),'ro');
    if max(abs(Rq))>= 1,
        disp(['Unstable filter for ',num2str(Bits),' Bits, Freq Resp Invalid!']);
        title(['Unstable Poles for ',num2str(Bits),' Bits']);
    else
        title(['Poles for ',num2str(Bits),' Bits']);
    end;
    hold on;grid on;
    R=(roots(A));
    polar(angle(R),abs(R),'b*');
 end;